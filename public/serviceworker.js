importScripts('/async-waituntil.js');

const version = 'v0.001::';
const staticCacheName = version + 'static';

function updateStaticCache() {
  return caches.open(staticCacheName)
    .then( cache => {
    // These items won't block the installation of the Service Worker
    cache.addAll([
      '/favicon.png'
    ]);
    // These items must be cached for the Service Worker to complete installation
    return cache.addAll([
      '/',
      '/index.html',
      '/hosted.html',
      '/thirdparty/require.min.js',
 '/manifest.json',
 '/hosted.js',
 '/favicon.png',
 '/bramble.js',
 '/xorigin.js',
 '/thirdparty/CodeMirror/lib/codemirror.css',
 '/styles/brackets.min.css',
 '/styles/bramble_overrides.css',
 '/thirdparty/thirdparty.min.js',
 '/thirdparty/require.min.js',
 '/dependencies.js',
 '/main.js',
 '/thirdparty/text/text.js',
 '/config.json',
 '/thirdparty/CodeMirror/mode/css/css.js',
 '/thirdparty/CodeMirror/mode/htmlmixed/htmlmixed.js',
 '/thirdparty/CodeMirror/mode/javascript/javascript.js',
 '/thirdparty/CodeMirror/mode/xml/xml.js',
 '/thirdparty/CodeMirror/mode/markdown/markdown.js',
 '/thirdparty/CodeMirror/mode/meta.js',
 '/styles/images/split-view-icons.svg',
 '/styles/images/topcoat-settings-13.svg',
 '/styles/images/live_development_sprites.svg',
 '/styles/images/extension-manager-sprite.svg',
 '/styles/images/updateSprites.svg',
 '/styles/images/no_content_bg.svg',
 '/styles/images/topcoat-inactive-15.svg',
 '/extensions/default/CSSCodeHints/main.js',
 '/extensions/default/HTMLCodeHints/main.js',
 '/thirdparty/text/text.js',
 '/extensions/default/JavaScriptCodeHints/main.js',
 '/extensions/default/CSSCodeHints/CSSProperties.json',
 '/extensions/default/HTMLCodeHints/HtmlTags.json',
 '/extensions/default/HTMLCodeHints/HtmlAttributes.json',
 '/extensions/default/JavaScriptCodeHints/ParameterHintManager.js',
 '/extensions/default/JavaScriptCodeHints/HintUtils.js',
 '/extensions/default/JavaScriptCodeHints/ScopeManager.js',
 '/extensions/default/JavaScriptCodeHints/Session.js',
 '/extensions/default/JavaScriptCodeHints/thirdparty/acorn/acorn.js',
 '/extensions/default/JavaScriptCodeHints/HintUtils2.js',
 '/thirdparty/text/text.js',
 '/extensions/default/JavaScriptCodeHints/MessageIds.js',
 '/extensions/default/JavaScriptCodeHints/Preferences.js',
 '/extensions/default/JavaScriptCodeHints/thirdparty/acorn/acorn_loose.js',
 '/extensions/default/JavaScriptCodeHints/ParameterHintTemplate.html',
 '/extensions/default/JavaScriptCodeHints/keyboard.json',
 '/extensions/default/JavaScriptCodeHints/thirdparty/tern/defs/ecma5.json',
 '/extensions/default/JavaScriptCodeHints/thirdparty/tern/defs/browser.json',
 '/extensions/default/InlineColorEditor/main.js',
 '/extensions/default/InlineColorEditor/InlineColorEditor.js',
 '/extensions/default/InlineColorEditor/ColorEditor.js',
 '/extensions/default/JavaScriptQuickEdit/main.js',
 '/extensions/default/InlineColorEditor/thirdparty/tinycolor-min.js',
 '/thirdparty/text/text.js',
 '/extensions/default/InlineColorEditor/ColorEditorTemplate.html',
 '/extensions/default/InlineColorEditor/css/main.css',
 '/extensions/default/QuickOpenCSS/main.js',
 '/extensions/default/QuickOpenHTML/main.js',
 '/extensions/default/QuickOpenJavaScript/main.js',
 '/extensions/default/QuickView/main.js',
 '/extensions/default/WebPlatformDocs/main.js',
 '/thirdparty/text/text.js',
 '/extensions/default/WebPlatformDocs/InlineDocsViewer.js',
 '/extensions/default/QuickView/QuickViewTemplate.html',
 '/thirdparty/text/text.js',
 '/extensions/default/QuickView/QuickView.css',
 '/extensions/default/WebPlatformDocs/InlineDocsViewer.html',
 '/extensions/default/WebPlatformDocs/WebPlatformDocs.css',
 '/extensions/default/CodeFolding/main.js',
 '/extensions/default/CodeFolding/Prefs.js',
 '/extensions/default/CodeFolding/foldhelpers/foldgutter.js',
 '/extensions/default/CodeFolding/foldhelpers/foldcode.js',
 '/extensions/default/CodeFolding/foldhelpers/indentFold.js',
 '/extensions/default/CodeFolding/main.css',
 '/thirdparty/CodeMirror/addon/fold/brace-fold.js',
 '/thirdparty/CodeMirror/addon/fold/comment-fold.js',
 '/thirdparty/CodeMirror/addon/fold/markdown-fold.js',
 '/extensions/default/bramble/main.js',
 '/extensions/default/bramble/lib/iframe-browser.js',
 '/extensions/default/bramble/lib/UI.js',
 '/extensions/default/bramble/lib/launcher.js',
 '/extensions/default/bramble/nohost/main.js',
 '/extensions/default/bramble/lib/PostMessageTransport.js',
 '/extensions/default/bramble/lib/xhr/XHRHandler.js',
 '/extensions/default/bramble/lib/Theme.js',
 '/extensions/default/bramble/lib/RemoteCommandHandler.js',
 '/extensions/default/bramble/lib/RemoteEvents.js',
 '/extensions/default/bramble/lib/compatibility.js',
 '/extensions/default/bramble/lib/Tutorial.js',
 '/thirdparty/text/text.js',
 '/extensions/default/bramble/nohost/HTMLServer.js',
 '/extensions/default/bramble/nohost/StaticServer.js',
 '/extensions/default/bramble/lib/MouseManager.js',
 '/extensions/default/bramble/lib/LinkManager.js',
 '/extensions/default/bramble/lib/Mobile.html',
 '/extensions/default/bramble/lib/PostMessageTransportRemote.js',
 '/extensions/default/bramble/lib/xhr/XHRShim.js',
 '/extensions/default/bramble/lib/MouseManagerRemote.js',
 '/extensions/default/bramble/lib/LinkManagerRemote.js',
 '/extensions/default/bramble/stylesheets/style.css',
 '/extensions/default/bramble/stylesheets/sidebarTheme.css',
 '/extensions/default/Autosave/main.js',
 '/extensions/default/brackets-paste-and-indent/main.js',
 '/extensions/default/BrambleUrlCodeHints/main.js',
 '/extensions/default/BrambleUrlCodeHints/camera/index.js',
 '/extensions/default/BrambleUrlCodeHints/selfie.js',
 '/thirdparty/text/text.js',
 '/extensions/default/BrambleUrlCodeHints/camera/interface.js',
 '/extensions/default/BrambleUrlCodeHints/camera/video.js',
 '/extensions/default/BrambleUrlCodeHints/camera/photo.js',
 '/extensions/default/BrambleUrlCodeHints/camera/utils.js',
 '/extensions/default/BrambleUrlCodeHints/camera-dialog.js',
 '/extensions/default/BrambleUrlCodeHints/data.json',
 '/extensions/default/BrambleUrlCodeHints/camera/selfieWidget.html',
 '/extensions/default/UploadFiles/main.js',
 '/extensions/default/BrambleUrlCodeHints/dialog.html',
 '/extensions/default/UploadFiles/UploadFilesDialog.js',
 '/extensions/default/BrambleUrlCodeHints/style.css',
 '/thirdparty/text/text.js',
 '/extensions/default/UploadFiles/htmlContent/upload-files-dialog.html',
 '/extensions/default/UploadFiles/styles.css',
 '/extensions/default/bramble-move-file/main.js',
 '/extensions/default/bramble-move-file/MoveToDialog.js',
 '/extensions/default/bramble-move-file/MoveUtils.js',
 '/thirdparty/text/text.js',
 '/extensions/default/bramble-move-file/htmlContent/move-to-dialog.html',
 '/extensions/default/bramble-move-file/htmlContent/directory-tree.html',
 '/extensions/default/bramble-move-file/styles/style.css',
 '/styles/images/jsTreeSprites.svg',
 '/extensions/default/CSSCodeHints/styles/brackets-css-hints.css',
 '/extensions/default/JavaScriptCodeHints/styles/brackets-js-hints.css',
 '/extensions/default/bramble/stylesheets/lightTheme.css',
 '/extensions/default/bramble/stylesheets/darkTheme.css',
 '/styles/images/no_content_bg_dark.svg',
 '/extensions/default/JavaScriptCodeHints/tern-worker.js',
 '/thirdparty/require.min.js',
 '/extensions/default/JavaScriptCodeHints/MessageIds.js',
 '/extensions/default/JavaScriptCodeHints/HintUtils2.js',
 '/extensions/default/JavaScriptCodeHints/thirdparty/tern/lib/tern.js',
 '/extensions/default/JavaScriptCodeHints/thirdparty/tern/lib/infer.js',
 '/extensions/default/JavaScriptCodeHints/thirdparty/acorn/acorn.js',
 '/extensions/default/JavaScriptCodeHints/thirdparty/acorn/acorn_loose.js',
 '/extensions/default/JavaScriptCodeHints/thirdparty/acorn/util/walk.js',
 '/extensions/default/JavaScriptCodeHints/thirdparty/tern/lib/def.js',
 '/extensions/default/JavaScriptCodeHints/thirdparty/tern/lib/signal.js'
    ]);
  });
}

// Remove caches whose name is no longer valid
function clearOldCaches() {
  return caches.keys()
    .then( keys => {
    return Promise.all(keys
                       .filter(key => key.indexOf(version) !== 0)
                       .map(key => caches.delete(key))
                      );
  });
}

self.addEventListener('install', event => {
  event.waitUntil(
    updateStaticCache()
    .then( () => self.skipWaiting() )
  );
});

self.addEventListener('activate', event => {
  event.waitUntil(
    clearOldCaches()
    .then( () => self.clients.claim() )
  );
});

self.addEventListener('fetch', event => {
  let request = event.request;
  // Look in the cache first, fall back to the network
  event.respondWith(
    // CACHE
    caches.match(request)
    .then( responseFromCache => {
      // Did we find the file in the cache?
      if (responseFromCache) {
        // If so, fetch a fresh copy from the network in the background
        // (using the async waitUntil polyfill)
        event.waitUntil(
          // NETWORK
          fetch(request)
          .then( responseFromFetch => {
            // Stash the fresh copy in the cache
            caches.open(staticCacheName)
              .then( cache => {
              cache.put(request, responseFromFetch);
            });
          })
        );
        return responseFromCache;
      }
      // NETWORK
      // If the file wasn't in the cache, make a network request
      fetch(request)
        .then( responseFromFetch => {
        // Stash a fresh copy in the cache in the background
        // (using the async waitUntil polyfill)
        let responseCopy = responseFromFetch.clone();
        event.waitUntil(
          caches.open(staticCacheName)
          .then( cache => {
            cache.put(request, responseCopy);
          })
        );
        return responseFromFetch;
      })
    })
  );
});
