'use strict';

if ('serviceWorker' in navigator) {
    // If service workers are supported
    navigator.serviceWorker.register('/serviceworker.js');
}
